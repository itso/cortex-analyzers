#!/usr/bin/env python3

from cortexutils.analyzer import Analyzer


class AnalyzerQuickstart(Analyzer):
    def __init__(self):
        Analyzer.__init__(self)

        self.service = self.get_param('config.service', None)  # retrieves config.service from the analyzer json
        self.observable = self.get_param('data')  # the actual observable being analyzed

        self.example_param = self.get_param('config.example_param')
        self.example_optional_param = self.get_param('config.example_optional_param', None)

    def run(self):
        # run() is generally the main part of the analyzer, though you can just call other classes or functions instead.
        # The important thing is that you eventually call self.report() with a dict object to report on

        # Do your main analyzer stuff here to get data for the results variable.
        # Below is just example data to generate a report.
        results = {
            'artifacts': [
                {'type': 'ip', 'value': '192.0.2.100'},
                {'type': 'domain', 'value': 'example.com'},
                {'type': 'url', 'value': 'https://example.com'}
            ],
            'text': 'Example text, lorem ipsum, results go here, etc.',
            'list': ['Example list', 'lorem ipsum', 'results go here', 'etc.']
        }

        # self.report({ 'results': results }) will call self.summary, then self.artifacts, and then will print json to stdout that looks like:
        # {
        #     'success': True,
        #     'summary': summary,
        #     'artifacts': artifacts,
        #     'full': results
        # }
        # self.report() should be basically the last thing that happens inside the run() function
        self.report({
                     'results': results,  # this doesn't actually all have to be in one key:value, but I find it helps organize
                     'input': self._input,  # you should probably remove this, but it helps for development and troubleshooting
                    })

    def summary(self, raw):
        # summary() is where the short report is created, mostly for display as taxonomy tags.
        # The 'raw' argument here is whatever object is passed to self.report()

        taxonomies = []  # each taxonomy in this list will show up as an individual tag
        namespace = 'Quickstart'  # generally this should be unique to the analyzer

        predicate = 'Confidence'  # This should be the name of a key bit of information the analyzer is trying to get across
        confidence = raw.get('confidence', None)  # This should be the actual value of that information
        if confidence:
            if confidence > 70:
                level = 'malicious'  # the level variable can be used in the short template to color the tag based on severity
            elif confidence > 40:
                level = 'suspicious'
            elif confidence > 10:
                level = 'safe'
            else:
                level = 'info'
            taxonomies.append(self.build_taxonomy(level, namespace, predicate, confidence))

        # Using the short.html report template included with this example, the following would create a yellow
        # taxonomy tag that says Quickstart:Confidence="55"
        taxonomies.append(self.build_taxonomy(level='suspicious',
                                              namespace='Quickstart',
                                              predicate='Confidence',
                                              value=55))

        return {"taxonomies": taxonomies}

    def artifacts(self, raw):
        # artifacts() is where you get the extracted observables under "Show observables" in TheHive.
        # The 'raw' argument here is whatever object is passed to self.report()

        artifacts = []

        for artifact in raw['results']['artifacts']:
            artifacts.append({'type': artifact['type'], 'value': artifact['value']})

        return(artifacts)


if __name__ == '__main__':
    AnalyzerQuickstart().run()
