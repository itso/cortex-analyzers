#!/usr/bin/env python3

import requests

from urllib.parse import urljoin, quote
from cortexutils.analyzer import Analyzer


class FalconIntelligence(Analyzer):
    def __init__(self):
        Analyzer.__init__(self)
        self.service = self.get_param('config.service', None, 'Service parameter is missing')
        self.ioc = self.getParam('data')
        self.item = {}

        cs_custid = self.get_param('config.cs_custid')
        cs_custkey = self.get_param('config.cs_custkey')
        self.cs_api_url = self.get_param('config.cs_api_url')
        self.cs_headers = {
                        'Content-Type': 'application/json',
                        'X-CSIX-CUSTID': cs_custid,
                        'X-CSIX-CUSTKEY': cs_custkey
                    }

    def search_ioc(self, ioc):
        request_url = urljoin(self.cs_api_url, '/indicator/v2/search/indicator/?equal={0}&sort=malicious_confidence&order=desc'.format(quote(ioc, safe='')))
        r = requests.get(url=request_url, headers=self.cs_headers)
        if r.status_code == 200:
            for item in r.json():

                # everything comes with a million hash relations, too noisy
                temp_relations = []
                for relation in item.get('relations', []):
                    if not relation.get('type', '').startswith('hash_'):
                        temp_relations.append(relation)
                item['relations'] = temp_relations

                return(item)
        else:
            self.error('Error getting details for IOC: {0}, {1}'.format(r.status_code, r.text))
            return({})
        return({})

    def summary(self, raw):
        taxonomies = []
        namespace = 'FalconIntelligence'

        if self.service in ['observable']:
            predicate = 'Confidence'
            confidence = self.item.get('malicious_confidence', None)
            if confidence:
                if confidence == 'high':
                    level = 'malicious'
                elif confidence == 'medium':
                    level = 'malicious'
                elif confidence == 'low':
                    level = 'suspicious'
                elif confidence == 'unverified':
                    level = 'suspicious'
                else:
                    level = 'safe'
                taxonomies.append(self.build_taxonomy(level, namespace, predicate, confidence))

        return {"taxonomies": taxonomies}

    def artifacts(self, raw):
        artifacts = []
        relations = self.item.get('relations', [])

        for relation in relations:
            indicator = relation.get('indicator')
            type = relation.get('type')

            type_map = {
                'domain': 'domain',
                'email_address': 'mail',
                'email_subject': 'mail_subject',
                'file_name': 'filename',
                'hash_md5': 'hash',
                'hash_sha256': 'hash',
                'ip_address': 'ip',
                'registry': 'registry',
                'url': 'url',
                'user_agent': 'user-agent'
            }

            if type in type_map:
                artifacts.append({'type': type_map[type], 'value': indicator})

        return(artifacts)

    def run(self):
        self.item = self.search_ioc(self.ioc)

        self.report(self.item)


if __name__ == '__main__':
    FalconIntelligence().run()
